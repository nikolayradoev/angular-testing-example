import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(waitForAsync(() => {
    // Syntaxe alternative pour tester un composant
    // N'est pas nécessaire pour un standalone Component
    TestBed.configureTestingModule({
      imports: [AppComponent],
    }).compileComponents();

    // Problème : les Components enfants sont des vraies instances et peuvent causer des problèmes
    // Voir la console et le message du constructeur de MouseComponent
    // Solution : voir le fichier app.component.standalone.spec.ts

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
  }));

  it('should create the component', () => {
    console.error('Problème avec les tests d\'AppComponent');
    expect(component).toBeTruthy();
  });
});
