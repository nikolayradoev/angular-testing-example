import { Component } from '@angular/core';
import { BadMouseComponent } from './bad-mouse/bad-mouse.component';
import { MouseComponent } from './mouse/mouse.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    imports: [BadMouseComponent, MouseComponent]
})
export class AppComponent {

}
