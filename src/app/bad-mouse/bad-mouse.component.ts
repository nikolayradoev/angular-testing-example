import { Component } from '@angular/core';

@Component({
  selector: 'app-bad-mouse',
  template: `
  <canvas id="badMouseCanvas"
        [attr.height]="height" 
        [attr.width]="width" 
        [style.background]="color" 
        (mousedown)="onMouseDown($event)" 
        (mouseup)="onMouseUp($event)">
  </canvas>
  `
})
export class BadMouseComponent {

  color = 'red';
  width = 500;
  height = 500;

  // Variables peuvent être mieux regroupées dans un objet
  startX = 0;
  startY = 0;
  endX = 0;
  endY = 0;

  constructor() { 
    console.error(`BadMouse : Ce message ne devra pas être visible dans les tests`);
  }

  onMouseDown(event: MouseEvent) {

    this.startX = event.offsetX;
    this.startY = event.offsetY;
    console.log(`Mouse Down on x: ${this.startX} y: ${this.startY}`);
  }

  onMouseUp(event: MouseEvent) {

    this.endX = event.offsetX;
    this.endY = event.offsetY;
    console.log(`Mouse Up on x: ${this.endX} y: ${this.endY}`);

    console.log(`Total distance : ${this.calculateDistance()}`);
  }

  // Beaucoup de couplage fort
  // Besoin de 4 variables et de l'état interne de l'objet
  calculateDistance(): number {
    const distanceX = Math.abs(this.endX - this.startX);
    const distanceY = Math.abs(this.endY - this.startY);

    const totalDistance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

    return totalDistance;
  }

  /**
   * Retourne la distance euclidienne entre deux points.
   * @param startX X coordonnée X du premier point.
   * @param startY Y coordonnée Y du premier point.
   * @param endX X coordonnée X du deuxième point.
   * @param endY Y coordonnée Y du deuxième point.
   * @returns Distance entre les deux points
   */
  calculateDistancePure(startX: number, startY: number, endX: number, endY: number): number {
    const distanceX = Math.abs(endX - startX);
    const distanceY = Math.abs(endY - startY);

    const totalDistance = Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));

    return totalDistance;
  }
}
