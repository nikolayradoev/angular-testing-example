import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BadMouseComponent } from './bad-mouse.component';

describe('BadMouseComponent', () => {
  let component: BadMouseComponent;
  let fixture: ComponentFixture<BadMouseComponent>;

  beforeEach(() => {
    // Mettre la console en "silence"
    spyOn(console, 'error').and.callFake(() => { });
    spyOn(console, 'log').and.callFake(() => { });

    fixture = TestBed.createComponent(BadMouseComponent);
    component = fixture.componentInstance;
    fixture.autoDetectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onMouseDown should read the mouse pointer position', () => {
    const position = 100;
    const mockClick = new MouseEvent('mousedown', {
      clientX: position,
      clientY: position,
    });

    component.onMouseDown(mockClick);

    expect(component.startX).toBe(position);
    expect(component.startY).toBe(position);
  });

  it('onMouseUp should read the mouse pointer position', () => {
    const position = 100;
    const mockClick = new MouseEvent('mouseup', {
      clientX: position,
      clientY: position,
    });

    component.onMouseUp(mockClick);

    expect(component.endX).toBe(position);
    expect(component.endY).toBe(position);
  });

  it('should calculate distance between two points (STRONG COUPLING)', () => {
    const startPosition = { x: 0, y: 0 };
    const endPosition = { x: 3, y: 4 };
    const distance = 5;

    component.startX = startPosition.x;
    component.startY = startPosition.y;
    component.endX = endPosition.x;
    component.endY = endPosition.y;

    // Le résultat dépend de l'état internes de l'objet
    expect(component.calculateDistance()).toBe(distance);
  });

  it('should calculate distance between two points (WEAK COUPLING)', () => {
    const startPosition = { x: 0, y: 0 };
    const endPosition = { x: 3, y: 4 };
    const distance = 5;

    // Ne dépend pas des autres méthodes. Peut être testée avant
    const calculatedDistance =
      component.calculateDistancePure(
        startPosition.x, startPosition.y,
        endPosition.x, endPosition.y
      );

    expect(calculatedDistance).toBe(distance);
  });

  it('should always calculate a positive distance', () => {
    // Duplication de code. Ces variables dévraient être déclarées à 1 seul endroit
    const startPosition = { x: 3, y: 4 };
    const endPosition = { x: 0, y: 0 };
    const distance = 5;

    const calculatedDistance =
      component.calculateDistancePure(
        startPosition.x, startPosition.y,
        endPosition.x, endPosition.y
      );

    expect(calculatedDistance).toBeGreaterThanOrEqual(0);
    expect(calculatedDistance).toBe(distance);
  });

});

