import { TestBed } from '@angular/core/testing';

import { MouseHandlerService } from './mouse-handler.service';
import { Coordinate } from './coordinate';

describe('MouseHandlerService', () => {
  let service: MouseHandlerService;
  let startPosition: Coordinate;
  let endPosition: Coordinate;
  let distance: number;

  // Exécuté avant chaque test avec une nouvelle instance de service
  beforeEach(() => {
    // Configuration pas strictement nécessaire pour ce test
    TestBed.configureTestingModule({ providers: [MouseHandlerService] });

    service = TestBed.inject(MouseHandlerService);
    startPosition = { x: 0, y: 0 };
    endPosition = { x: 3, y: 4 };
    distance = 5;

    spyOn(console, 'log').and.callFake(() => { });
  });

  it('onMouseDown should read the mouse pointer position', () => {
    service.onMouseDown(startPosition);

    expect(service.startCoordinate.x).toBe(startPosition.x);
    expect(service.startCoordinate.y).toBe(startPosition.y);
  });

  it('onMouseUp should read the mouse pointer position', () => {
    service.onMouseUp(endPosition);

    expect(service.endCoordinate.x).toBe(endPosition.x);
    expect(service.endCoordinate.y).toBe(endPosition.y);
  });

  it('should calculate distance between two points', () => {
    const calculatedDistance = service.calculateDistance(startPosition, endPosition);

    expect(calculatedDistance).toBe(distance);
  });


  it('should always have a positive or null distance', () => {
    // Variables rédéfinies pour les besoins du test
    endPosition = { x: -3, y: -4 };

    const calculatedDistance = service.calculateDistance(startPosition, endPosition);

    expect(calculatedDistance).toBeGreaterThanOrEqual(0);
    expect(calculatedDistance).toBe(distance);
  });

  it('should calculate a distance of 0 if there\'s no movement ', () => {
    startPosition = { x: 3, y: 4 };
    distance = 0;

    const calculatedDistance = service.calculateDistance(startPosition, endPosition);

    expect(calculatedDistance).toBe(distance);
  });

  it('should call calculateDistance when wrapper function is called ', () => {
    startPosition = { x: 3, y: 4 };
    service.startCoordinate = startPosition;
    service.endCoordinate = endPosition;

    // On espionne la vraie méthode avec un appel vers une fausse fonction
    // Un vrai appel serait fait avec .callThrough();
    const spy = spyOn(service, 'calculateDistance').and.callFake((x, y) => 10);

    service.calculateDistanceWrapper();

    // On peut détecter l'appel ainsi que les arguments passés
    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith(service.startCoordinate, service.endCoordinate);

  });

  it('Spy should change what is called when wrapper function is called ', () => {
    service.startCoordinate = startPosition;
    service.endCoordinate = endPosition;

    // Note : printToConsole est privée donc <any> est nécessaire pour l'espion
    const spy = spyOn<any>(service, 'printToConsole').and.callFake((x: number) => { });

    const calculatedDistance = service.calculateDistanceWrapper();

    expect(spy).toHaveBeenCalled();
    expect(calculatedDistance).toBe(5);
  });

});
