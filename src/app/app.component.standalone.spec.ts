import { ComponentFixture, MetadataOverride, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Component } from '@angular/core';
import { MouseComponent } from './mouse/mouse.component';
import { BadMouseComponent } from './bad-mouse/bad-mouse.component';

@Component({
  selector: 'app-bad-mouse',
  standalone: true,
  template: ''
})
class MockBadMouseComponent { }

@Component({
  selector: 'app-mouse',
  standalone: true,
  template: ''
})
class MockMouseComponent { }

describe('AppComponent Standalone', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(() => {
    //Solution : remplacer le Component enfant par un Mock
    const overrideInfo: MetadataOverride<AppComponent> = {
      add: { imports: [MockBadMouseComponent] },
      remove: { imports: [BadMouseComponent] }
    };
    TestBed.overrideComponent(AppComponent, overrideInfo);

    // Même chose pour le MouseComponent
    TestBed.overrideComponent(AppComponent, {
      add: { imports: [MockMouseComponent] },
      remove: { imports: [MouseComponent] }
    });

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;

  });

  it('should create the component without dependencies', () => {
    expect(component).toBeTruthy();
  });
});
