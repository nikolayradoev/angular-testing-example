import { Component, inject } from '@angular/core';
import { Coordinate } from '../services/coordinate';
import { MouseHandlerService } from '../services/mouse-handler.service';

@Component({
  selector: 'app-mouse',
  template: `
  <canvas id="mouseCanvas"
        [attr.height]="height" 
        [attr.width]="width" 
        [style.background]="color" 
        (mousedown)="onMouseDown($event)" 
        (mouseup)="onMouseUp($event)">
  </canvas>
  `
})
export class MouseComponent {

  color = 'green';
  width = 500;
  height = 500;

  private mouseService = inject(MouseHandlerService);
  
  onMouseDown(event: MouseEvent) {
    const coordinateClick: Coordinate = { x: event.offsetX, y: event.offsetY };
    this.mouseService.onMouseDown(coordinateClick);
  }

  onMouseUp(event: MouseEvent) {
    const coordinateClick: Coordinate = { x: event.offsetX, y: event.offsetY };
    this.mouseService.onMouseUp(coordinateClick);
  }

}
