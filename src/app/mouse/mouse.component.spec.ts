import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MouseComponent } from './mouse.component';

import SpyObj = jasmine.SpyObj;
import { MouseHandlerService } from '../services/mouse-handler.service';


describe('MouseComponent', () => {

  let mouseServiceSpy: SpyObj<MouseHandlerService>;
  let component: MouseComponent;
  let fixture: ComponentFixture<MouseComponent>;

  beforeEach(() => {
    /*  Création d'un faux service qu'on peut espionner. Le service est injecté grâce à la méthode overrideProvider.
        Les méthodes espionnées sont vides et ont la même signature que celles du service réel.
        Un problème potentiel avec le service n'impactera les tests de notre Component.
    */
    mouseServiceSpy = jasmine.createSpyObj('MouseHandlerService', ['onMouseDown', 'onMouseUp']);
    TestBed.overrideProvider(MouseHandlerService, { useValue: mouseServiceSpy });

    fixture = TestBed.createComponent(MouseComponent);
    component = fixture.componentInstance;
    fixture.autoDetectChanges();

  });

  it('Pushing the mouse button down should call onMouseDown in the service', () => {
    const mockClick = new MouseEvent('mousedown');

    component.onMouseDown(mockClick);
    expect(mouseServiceSpy.onMouseDown).toHaveBeenCalled();
  });

  it('Letting the mouse button up should call onMouseUp in the service', () => {
    const mockClick = new MouseEvent('mouseup');

    component.onMouseUp(mockClick);
    expect(mouseServiceSpy.onMouseUp).toHaveBeenCalled();
  });

});
