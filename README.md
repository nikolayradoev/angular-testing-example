# Exemple de tests avec Angular

Ceci est un projet simple visant à démontrer comment écrire des tests pour des projets Angular avec la librairie `Jasmine`.
Pour plus de détails, consultez la documentation d'Angular qui va plus en profondeurs sur les stratégies de tests. Les tutoriels sont disponibles [ICI](https://angular.dev/guide/testing). Consultez [la base des tests des Components](https://angular.dev/guide/testing/components-basics) et [comment tests les Services](https://angular.dev/guide/testing/services) pour plus d'informations.

## Angular 18 et `standalone` Components

Cette version du projet utilise la nouvelle syntaxe de `standalone` Components d'Angular. Cette syntaxe permet de créer des Components qui ne sont pas liés à un module et qui peuvent être utilisés dans d'autres Components sans être importés dans le module. 

La différence entre la nouvelle syntaxe et les modules n'est pas grande, mais peut avoir un impact, surtout au niveau des tests de Components ayant d'autre `standalone` Components comme enfant. Des branches utilisant Angular 12 et les modules sont disponibles pour comparer les différences. Notez que ces versions sont obsolètes et ne sont plus maintenues.

## Lancement du projet

Exécutez `npm start` pour un serveur de développement local. Naviguez sur `http://localhost:4200/`. Le site rechargera automatiquement si vous modifiez le code source.

## Lancement des tests unitaires

Exécutez `npm test` pour exécuter les tests unitaires via [Karma](https://karma-runner.github.io). Une fênetre de navigateur s'ouvrira pour afficher les résultats des tests. Les tests sont exécutés à chaque modification du code source.

Une sortie dans la console du terminer est aussi disponible pour voir les résultats des tests.

## Couverture du code

Exécutez `npm run coverage` pour exécuter les tests unitaires et calculer la couverture du code. La même fenêtre de navigateur s'ouvrira pour afficher les résultats des tests.


Vous pouvez accéder à `index.html` dans le répertoire **coverage/angular-testing-example** pour explorer le rapport sur la couverture du code.

# Fichiers principaux du projet

Le projet contient 2 Components qui implémentent la même fonctionnalité : gérer les événements de la souris et calculer la distance entre l'événement d'appui et relâchement de la souris. Les informations sont imprimées dans la console.

## [Bad Mouse Component](./src/app/bad-mouse/bad-mouse.component.ts)

Implémente l'ensemble de la logique dans la même classe : le Component.
Ceci est un exemple des erreurs potentielles lorsqu'on mélange la logique d'affichage et la logique de traitement ainsi que la difficulté supplémentaire lors des tests.

**Note:** l'utilisation de seulement un Component sans Service n'est pas nécessairement une mauvaise pratique. Tout dépend de votre projet et vos besoins.

### Configuration de base pour les tests

Pour tester un Component, il faut importer le module `TestBed` de `@angular/core/testing` et le Component à tester. Dans un cas simple comme ici, il n'y a pas de dépendances à remplacer et on peut simplement importer le Component à tester.

```ts
let component: BadMouseComponent;
let fixture: ComponentFixture<BadMouseComponent>;

beforeEach(() => {
    // Mettre la console en "silence"
    spyOn(console, 'error').and.callFake(() => { });
    spyOn(console, 'log').and.callFake(() => { });

    fixture = TestBed.createComponent(BadMouseComponent);
    component = fixture.componentInstance;
    fixture.autoDetectChanges();
});
```

## [Mouse Component](./src/app/mouse/mouse.component.ts) and [MouseHandler Service](./src/app/services/mouse-handler.service.ts)

Implémente la logique de traitement dans une classe séparée : un Service.
Ceci est un exemple des avantages de la division de la logique d'affichage et la logique de traitement ainsi que la manière de tester cette approche

Voir [`mouse.component.spec.ts`](./src/app/mouse/mouse.component.spec.ts) pour un exemple de comment utiliser un `Jasmine Spy` comme un `Mock` pour replacer une dépendance lors d'un test.

Voir [`mouse-handler.service.spec.ts`](./src/app/services/mouse-handler.service.spec.ts) pour un exemple de comment utiliser un `Jasmine Spy` comme un `Spy` sur une vraie fonction.

### Configuration avec des dépendances

Pour tester un Component qui a des dépendances de manière unitaire, il faut remplacer ses dépendances par des `Mocks` ou des `Spies`. Dans le cas d'un service, il faut simplement remplacer le service dans le `TestBed` par un `SpyObject` qui a les mêmes fonctions.

À noter que dans le cas de `Jasmine`, un `SpyObject` est en fait un `Mock` puisque ses fonctions n'ont aucune implémentation.

```ts
describe('MouseComponent', () => {

  let mouseServiceSpy: SpyObj<MouseHandlerService>;
  let component: MouseComponent;
  let fixture: ComponentFixture<MouseComponent>;

  beforeEach(() => {
    mouseServiceSpy = jasmine.createSpyObj('MouseHandlerService', ['onMouseDown', 'onMouseUp']);
    TestBed.overrideProvider(MouseHandlerService, { useValue: mouseServiceSpy });

    fixture = TestBed.createComponent(MouseComponent);
    component = fixture.componentInstance;
    fixture.autoDetectChanges();

  });
});
```
Dans le cas du service, il n'y a pas d'autres dépendances, mais il est quand même posible d'utiliser le `TestBed` pour la configuration des tests.

```ts
beforeEach(() => {
    TestBed.configureTestingModule({ providers: [MouseHandlerService] });
    service = TestBed.inject(MouseHandlerService);
  });
```

### Espions sur les fonctions

Un `Spy` est un objet qui permet de surveiller l'appel d'une fonction et de retourner une valeur spécifique. Il est possible de vérifier si une fonction a été appelée, combien de fois et avec quels paramètres. Il est possible de rediriger l'appel de la fonction vers une autre fonction avec `callFake(newFunction)` : 

```ts
it('should call calculateDistance when wrapper function is called ', () => {
    startPosition = { x: 3, y: 4 };
    service.startCoordinate = startPosition;
    service.endCoordinate = endPosition;

    const spy = spyOn(service, 'calculateDistance')
        .and.callFake((x, y) => 10);

    service.calculateDistanceWrapper();

    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith(
        service.startCoordinate, service.endCoordinate);
  });
```

Un `Spy` peut être très pratique pour mettre les messages dans la console en "silence" pour éviter la pollution de la console lors des tests.

```ts
beforeEach(() => {
    ...
    spyOn(console, 'log').and.callFake(() => { });
  });
```

## [App Component](./src/app/app.component.ts)

Contient les 2 autres Components du projet. Possède donc deux dépendances externes : `BadMouseComponent` et `MouseComponent`.

Pour bien faire un test unitaire, il faut remplacer ces dépendances par des `Mocks`. Cependant, comme ce sont des Components `standalone`, les mocks ne peuvent pas être simplement ajoutés dans le `providers` ou `imports` du `TestBed`.  Le code suivant **n'est pas valide** :

```ts
@Component({
  selector: 'app-bad-mouse',
  standalone: true,
  template: ''
})
class MockBadMouseComponent { }

TestBed.configureTestingModule({
    imports: [AppComponent, MockBadMouseComponent],
}).compileComponents();
```

Voir le fichier [app.component.spec.ts](./src/app/app.component.spec.ts) pour un exemple qui illustre ce problème de vrai dépendances. Notez les sorties dans la console lors de l'exécution des tests.

Pour règler ce problème, il faut utiliser la méthode `TestBed.overrideComponent` pour remplacer les Components enfants par des `Mocks` dans son tableau `imports`. 

Voir le fichier [app.component.standalone.spec.ts](./src/app/app.component.standalone.spec.ts) pour un exemple de comment utiliser cette méthode : 

```ts
@Component({
  selector: 'app-bad-mouse',
  standalone: true,
  template: ''
})
class MockBadMouseComponent { }

const overrideInfo: MetadataOverride<AppComponent> = {
    add: { imports: [MockBadMouseComponent] },
    remove: { imports: [BadMouseComponent] }
};
TestBed.overrideComponent(AppComponent, overrideInfo);
```

# Exercices

Le but de l'exercice est d'expérimenter avec les différentes manières de communiquer entre Components disponibles avec Angular et leur impact au niveau de l'architecture du système ainsi que la manière de tester chaque approche.

## Nouvelle fonctionnalité 
Pour chaque exercice, l'objectif est d'implémenter une nouvelle fonctionnalité. Dans cette fonctionnalité, les données calculées par la gestion des événements de la souris (position de départ, de fin et position courante) ainsi que la distance entre les points de départ et fin sont affichées sur l'écran et non dans la Console. Pour l'ensemble des exercices, seulement le component `MouseComponent` sera considéré.

## Exercice 0

Implémenter la gestion de l'événement de déplacement de la souris [`mousemove`](https://developer.mozilla.org/en-US/docs/Web/API/Element/mousemove_event). La position de la souris est enregistrée dans `MouseHandlerService`.

## Exercice 1

Implémenter un nouveau Component (selector : `mouse-display`) qui permet d'afficher l'information pertinente (position de départ, position de fin et position courante). Ce component devra être un enfant de `MouseComponent` et l'information lui est passée à travers des propriétés utilisant le décorateur `@Input()`.

Utilisez le gabarit suivant :

```html
<p>Mouse Display</p>
<p>Start : {{start.x}} : {{start.y}}</p>
<p>End : {{end.x}} : {{end.y}}</p>
<p>Current : {{current.x}} : {{current.y}}</p>
```

## Exercice 2

Implémenter un Component similaire (selector : `mouse-component2`) qui permet d'afficher l'information pertinente (position de départ, position de fin et position courante). 

Le component doit récupérer l'information pertinente à travers `MouseHandlerService` sans avoir un lien de parenté direct avec `MouseComponent`.

Utilisez le même gabarit que l'exercice 1.

## Utilisation d'Angular CLI

Exécutez `ng generate component component-name` pour un nouveau component. 